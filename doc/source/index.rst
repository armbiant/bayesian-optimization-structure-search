.. BOSS documentation master file, created by
   sphinx-quickstart on Tue Dec 12 14:46:05 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BOSS
================================

.. toctree::
   :maxdepth: 4
   :hidden:

   whatis
   installation
   tutorials
   manual
   interface
   gallery
   citing
   about

.. .. image:: https://travis-ci.com/tolvana/boss-test.svg?branch=features
..     :target: https://travis-ci.com/tolvana

.. .. image:: https://codecov.io/gh/tolvana/boss-test/branch/features/graphs/badge.svg?branch=features
..     :target: http://codecov.io/gh/tolvana/boss-test?branch=features

Bayesian Optimization Structure Search (BOSS) is an active machine learning technique for accelerated global exploration of energy and property phase space. It is designed to facilitate machine learning in computational and experimental natural sciences.


Capabilities at a glance
+++++++++++++++++++++++++
BOSS builds surrogate models for materials properties with:

- robust GPR built on GPy with an easy input file
- restart capability for high-performance computing (HPC) platforms
- postprocessing with extensive visuals to track progress
- simple python interface to any computer code / property
- choice of kernels and acquisitions functions
- choice of priors on GP hyperparameters
- automated extraction of N-dimensional minima
- minimum energy path extraction built on RERT & NEB
