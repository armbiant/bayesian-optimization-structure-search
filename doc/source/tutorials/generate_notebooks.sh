#!/usr/bin/env sh
# Converts python scripts to ipython notebooks using jupytext
for f in pyfiles/*.py; do
  jupytext --to=ipynb $f;
done
mkdir -p notebooks
mv pyfiles/*.ipynb notebooks/
